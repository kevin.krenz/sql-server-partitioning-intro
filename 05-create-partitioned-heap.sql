USE PartitionIntro;
GO

DROP TABLE IF EXISTS dbo.OrdersHeapPartitioned;
GO

CREATE TABLE dbo.OrdersHeapPartitioned (
	OrderId int
	,OrderDate DATETIME
) ON PartitionByDecadeScheme(OrderDate);  
GO

INSERT INTO dbo.OrdersHeapPartitioned
SELECT OrderId
      ,OrderDate
FROM dbo.OrdersHeap;
GO



--Compare scans of entire table
SET STATISTICS IO ON;
GO

SELECT COUNT(*)
FROM dbo.OrdersHeap;

SELECT COUNT(*)
FROM dbo.OrdersHeapPartitioned;

GO

SELECT COUNT(*)
FROM dbo.OrdersHeap
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

SELECT COUNT(*)
FROM dbo.OrdersHeapPartitioned
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

GO

                   