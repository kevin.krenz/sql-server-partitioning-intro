USE PartitionIntro;
GO

DROP TABLE IF EXISTS dbo.OrdersColumnstorePartitioned;
GO

CREATE TABLE dbo.OrdersColumnstorePartitioned (
	 OrderId int
	,OrderDate DATETIME
	,INDEX ix_orders_columnstore_partitioned CLUSTERED COLUMNSTORE
) ON PartitionByDecadeScheme(OrderDate);  
GO

--CLUSTERED COLUMNSTORE
--Insert in batches to go straight into compressed rowgroups.
DECLARE @order_id AS int = 1;
DECLARE @batch_size AS int = 200000;
DECLARE @max_order_id as int = (SELECT MAX(OrderId) FROM dbo.OrdersRowstore);

WHILE @order_id < @max_order_id
BEGIN
	INSERT INTO dbo.OrdersColumnstorePartitioned
	SELECT OrderId
		  ,OrderDate
	FROM dbo.OrdersRowstore
	WHERE OrderId BETWEEN @order_id AND @order_id + @batch_size - 1;
	
	SET @order_id = @order_id + @batch_size;
END;
GO



SELECT *
FROM sys.column_store_row_groups
WHERE object_id = OBJECT_ID('OrdersColumnstorePartitioned');
GO



ALTER INDEX ix_orders_columnstore_partitioned on dbo.OrdersColumnstorePartitioned REORGANIZE
	WITH (COMPRESS_ALL_ROW_GROUPS = ON);
GO


--Compare scans of entire table
SET STATISTICS IO ON;
GO

SELECT COUNT(*)
FROM dbo.OrdersColumnstore;

SELECT COUNT(*)
FROM dbo.OrdersColumnstorePartitioned;
GO


SELECT COUNT(*)
FROM dbo.OrdersColumnstore
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

SELECT COUNT(*)
FROM dbo.OrdersColumnstorePartitioned
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';
GO
                   