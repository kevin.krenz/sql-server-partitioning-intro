chdir "c:\repos\sql-server-partitioning-intro"

sqlcmd -i "00-create_database.sql"
sqlcmd -i "01-create-orders-table.sql"
sqlcmd -i "02-load-nonpartitioned-tables.sql"
sqlcmd -i "03-baseline_queries.sql"
sqlcmd -i "04-create-partition-objects.sql"
sqlcmd -i "05-create-partitioned-heap.sql"
sqlcmd -i "06-create-partitioned-rowstore.sql"
sqlcmd -i "07-create-partitioned-columnstore.sql"
rem sqlcmd -i "08-switch-partitions.sql"
sqlcmd -i "09-switch-partitions-clean.sql"

pause