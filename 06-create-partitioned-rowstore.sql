USE PartitionIntro;
GO

DROP TABLE IF EXISTS dbo.OrdersRowstorePartitioned;
GO

CREATE TABLE dbo.OrdersRowstorePartitioned (
	 OrderId int
	,OrderDate DATETIME
	,INDEX ix_orders_rowstore_partitioned CLUSTERED (OrderDate)
) ON PartitionByDecadeScheme(OrderDate);  
GO

INSERT INTO dbo.OrdersRowstorePartitioned
SELECT OrderId
      ,OrderDate
FROM dbo.OrdersRowstore;

ALTER INDEX ix_orders_rowstore_partitioned on dbo.OrdersRowstorePartitioned REBUILD;
GO

--Compare scans of entire table
SET STATISTICS IO ON;
SELECT COUNT(*)
FROM dbo.OrdersRowstore;

SELECT COUNT(*)
FROM dbo.OrdersRowstorePartitioned;

GO


SELECT COUNT(*)
FROM dbo.OrdersRowstore
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

SELECT COUNT(*)
FROM dbo.OrdersRowstorePartitioned
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

GO

                   