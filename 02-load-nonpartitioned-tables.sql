USE PartitionIntro;
GO

--Create a numbers table to use
DROP TABLE IF EXISTS #numbers;
CREATE TABLE #numbers (
	Number int
);

INSERT INTO #numbers
SELECT 
		TOP (1000000)
		ROW_NUMBER() OVER (ORDER BY (SELECT NULL))
FROM master..spt_values a
CROSS JOIN master..spt_values b;
GO


--HEAP
INSERT INTO dbo.OrdersHeap
SELECT #numbers.Number
      ,DATEADD(DAY, -1 * (#numbers.Number % (365 * 10 * 6)), GETDATE())
FROM #numbers;
GO

--CLUSTERED ROWSTORE
INSERT INTO dbo.OrdersRowstore
SELECT OrderId
      ,OrderDate
FROM dbo.OrdersHeap;

--Rebuild the index since we inserted by ascending ID rather that date
ALTER INDEX ix_orders_rowstore ON dbo.OrdersRowstore REBUILD;
GO


--CLUSTERED COLUMNSTORE
--Insert in batches to go straight into compressed rowgroups.
DECLARE @order_id AS int = 1;
DECLARE @batch_size AS int = 200000;
DECLARE @max_order_id as int = (SELECT MAX(OrderId) FROM dbo.OrdersRowstore);

WHILE @order_id < @max_order_id
BEGIN
	INSERT INTO dbo.OrdersColumnstore
	SELECT OrderId
		  ,OrderDate
	FROM dbo.OrdersHeap
	WHERE OrderId BETWEEN @order_id AND @order_id + @batch_size - 1;
	
	SET @order_id = @order_id + @batch_size;
END;
GO

--SELECT *
--FROM sys.column_store_row_groups
--WHERE object_id = object_id('dbo.OrdersColumnstore');
