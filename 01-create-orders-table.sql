USE PartitionIntro;
GO

DROP TABLE IF EXISTS dbo.OrdersHeap;
CREATE TABLE dbo.OrdersHeap (
	 OrderId int
	,OrderDate DATETIME
);

DROP TABLE IF EXISTS dbo.OrdersRowstore;
CREATE TABLE dbo.OrdersRowstore (
	 OrderId int
	,OrderDate DATETIME
	,INDEX ix_orders_rowstore CLUSTERED (OrderDate)
);

DROP TABLE IF EXISTS dbo.OrdersColumnstore;
CREATE TABLE dbo.OrdersColumnstore (
	 OrderId int
	,OrderDate DATETIME
	,INDEX ix_orders_columnstore CLUSTERED COLUMNSTORE
);

GO

