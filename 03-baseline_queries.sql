USE PartitionIntro;
GO

SET STATISTICS IO ON;
GO

SELECT COUNT(*) 
FROM dbo.OrdersHeap;

SELECT COUNT(*) 
FROM dbo.OrdersRowstore;

SELECT COUNT(*) 
FROM dbo.OrdersColumnstore;



SELECT COUNT(*) 
FROM dbo.OrdersHeap        
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

SELECT COUNT(*) 
FROM dbo.OrdersRowstore    
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';

SELECT COUNT(*) 
FROM dbo.OrdersColumnstore 
WHERE OrderDate BETWEEN '1989-07-01' AND '1990-06-30';