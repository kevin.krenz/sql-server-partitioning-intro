USE PartitionIntro;
GO


--Create the stage table
DROP TABLE IF EXISTS dbo.Stage_Orders_1980;
CREATE TABLE Stage_Orders_1980 (
	OrderId int,
	OrderDate datetime,
	INDEX ix_stage_orders_1980 CLUSTERED COLUMNSTORE,
) ON Decade_1980;

ALTER TABLE dbo.Stage_Orders_1980
WITH CHECK ADD CONSTRAINT check_decade_1980 
CHECK (OrderDate IS NOT NULL 
       AND OrderDate >= '1980-01-01' 
	   AND OrderDate < '1990-01-01');
GO


--SWITCH OUT
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.OrdersColumnstorePartitioned
SWITCH PARTITION @partition_id_1980
TO Stage_Orders_1980

GO


--Modify the staged data

UPDATE dbo.Stage_Orders_1980
SET OrderId = OrderId * -1;

GO


--SWITCH IN
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.Stage_Orders_1980
	SWITCH TO dbo.OrdersColumnstorePartitioned PARTITION @partition_id_1980;

GO


--Verify
SELECT TOP (1000) *
FROM dbo.OrdersColumnstorePartitioned
WHERE OrderDate >= '1980-01-01'
AND OrderDate < '1990-01-01';

GO