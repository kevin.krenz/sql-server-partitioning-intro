USE PartitionIntro;
GO

IF EXISTS (SELECT 1 FROM sys.partition_functions where name = N'PartitionByDecadeFunction')
	DROP PARTITION FUNCTION PartitionByDecadeFunction;
GO

CREATE PARTITION FUNCTION PartitionByDecadeFunction (datetime)  
    AS RANGE RIGHT --"boundaries belong to the partition to the right"
	FOR VALUES ('1980-01-01', '1990-01-01', '2000-01-01', '2010-01-01');
GO  


IF EXISTS (SELECT 1 FROM sys.partition_schemes where name = N'PartitionByDecadeScheme')
	DROP PARTITION SCHEME PartitionByDecadeScheme;
GO


CREATE PARTITION SCHEME PartitionByDecadeScheme  
    AS PARTITION PartitionByDecadeFunction  
    TO ('Decade_Old', 'Decade_1980', 'Decade_1990', 'Decade_2000', 'Decade_2010'); 
GO  
