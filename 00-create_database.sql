USE master;
GO

DROP DATABASE IF EXISTS PartitionIntro;
GO

CREATE DATABASE PartitionIntro
	ON PRIMARY
	  ( NAME='PartitionIntro',
		FILENAME=
		   'C:\temp\partition_intro.mdf',
		SIZE=4MB,
		MAXSIZE=500MB,
		FILEGROWTH=1MB),
	FILEGROUP Decade_Old
	  ( NAME = 'Decade_Old_1',
		FILENAME =
		   'C:\temp\partition_decade_old_1.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	  ( NAME = 'Decade_Old_2',
		FILENAME =
		   'C:\temp\partition_decade_old_2.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	FILEGROUP Decade_1980
	  ( NAME = 'Decade_1980_1',
		FILENAME =
		   'C:\temp\partition_decade_1980_1.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	  ( NAME = 'Decade_1980_2',
		FILENAME =
		   'C:\temp\partition_decade_1980_2.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	FILEGROUP Decade_1990
	  ( NAME = 'Decade_1990_1',
		FILENAME =
		   'C:\temp\partition_decade_1990_1.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	  ( NAME = 'Decade_1990_2',
		FILENAME =
		   'C:\temp\partition_decade_1990_2.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	FILEGROUP Decade_2000
	  ( NAME = 'Decade_2000_1',
		FILENAME =
		   'C:\temp\partition_decade_2000_1.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	  ( NAME = 'Decade_2000_2',
		FILENAME =
		   'C:\temp\partition_decade_2000_2.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	FILEGROUP Decade_2010
	  ( NAME = 'Decade_2010_1',
		FILENAME =
		   'C:\temp\partition_decade_2010_1.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB),
	  ( NAME = 'Decade_2010_2',
		FILENAME =
		   'C:\temp\partition_decade_2010_2.ndf',
		SIZE = 1MB,
		MAXSIZE=25MB,
		FILEGROWTH=1MB)
	LOG ON
	  ( NAME='log',
		FILENAME =
		   'C:\temp\partition_log.ldf',
		SIZE=1MB,
		MAXSIZE=500MB,
		FILEGROWTH=1MB);
GO

ALTER DATABASE PartitionIntro SET RECOVERY SIMPLE;
GO

--USE PartitionIntro;
--GO

--select *
--from sys.filegroups;

--select *
--from sys.database_files;