# Feedback

SQLSaturday MN
2019-10-12

- [ ] Add visuals to first part of the presentation
- [ ] Increase size of database so that there is more than one rowgroup per partition
- [ ] Load columnstore by date
    - [ ] Show partition elimination no longer useful
    - [ ] Show that rebuilds blow it and make partitiong useful