USE PartitionIntro;
GO

--Can use partition function to find partition ID
SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30')

--And can directly narrow down to a partition
SELECT MIN(OrderDate) 'min'
	  ,MAX(OrderDate) 'max'
FROM dbo.OrdersColumnstorePartitioned
WHERE $PARTITION.PartitionByDecadeFunction(OrderDate) = 2;

GO


--Create a stage table
DROP TABLE IF EXISTS Stage_Orders_1980;
CREATE TABLE Stage_Orders_1980 (
	OrderId int,
	OrderDate datetime,
);

GO

--SWITCH OUT
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.OrdersColumnstorePartitioned
SWITCH PARTITION @partition_id_1980
TO Stage_Orders_1980;

GO












--Make table same structure
DROP TABLE IF EXISTS Stage_Orders_1980;
CREATE TABLE Stage_Orders_1980 (
	OrderId int,
	OrderDate datetime,
	INDEX ix_stage_orders_1980 CLUSTERED COLUMNSTORE,
);

GO

--SWITCH OUT
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.OrdersColumnstorePartitioned
SWITCH PARTITION @partition_id_1980
TO Stage_Orders_1980;

GO













--Create on same filegroup
DROP TABLE IF EXISTS Stage_Orders_1980;
CREATE TABLE Stage_Orders_1980 (
	OrderId int,
	OrderDate datetime,
	INDEX ix_stage_orders_1980 CLUSTERED COLUMNSTORE,
) ON Decade_1980;

GO

--SWITCH OUT
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.OrdersColumnstorePartitioned
SWITCH PARTITION @partition_id_1980
TO Stage_Orders_1980;

GO















--Success! Now do your modifications
UPDATE dbo.Stage_Orders_1980
SET OrderId = OrderId * -1;

GO








--SWITCH IN
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.Stage_Orders_1980
	SWITCH TO dbo.OrdersColumnstorePartitioned PARTITION @partition_id_1980;

GO







































--Add constraint on dates to stage table
ALTER TABLE Stage_Orders_1980
WITH CHECK ADD CONSTRAINT check_decade_1980 
CHECK (OrderDate BETWEEN '1980-01-01' AND '1990-01-01');
GO

--SWITCH IN
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.Stage_Orders_1980
	SWITCH TO dbo.OrdersColumnstorePartitioned PARTITION @partition_id_1980;

GO






















--Don't include right endpoint of interval
ALTER TABLE Stage_Orders_1980 
DROP CONSTRAINT check_decade_1980;;

ALTER TABLE Stage_Orders_1980
WITH CHECK ADD CONSTRAINT check_decade_1980 
CHECK (OrderDate >= '1980-01-01' 
	   AND OrderDate < '1990-01-01');
GO

--SWITCH IN
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.Stage_Orders_1980
	SWITCH TO dbo.OrdersColumnstorePartitioned PARTITION @partition_id_1980;

GO


















--Don't include NULLs
ALTER TABLE Stage_Orders_1980 
DROP CONSTRAINT check_decade_1980;;

ALTER TABLE Stage_Orders_1980
WITH CHECK ADD CONSTRAINT check_decade_1980 
CHECK (OrderDate IS NOT NULL 
       AND OrderDate >= '1980-01-01' 
	   AND OrderDate < '1990-01-01');
GO

--SWITCH IN
DECLARE @partition_id_1980 AS int = (SELECT $PARTITION.PartitionByDecadeFunction('1989-06-30'));

ALTER TABLE dbo.Stage_Orders_1980
	SWITCH TO dbo.OrdersColumnstorePartitioned PARTITION @partition_id_1980;

GO


















--Validate
SELECT COUNT(*)
FROM dbo.OrdersColumnstorePartitioned
WHERE OrderDate >= '1980-01-01' 
ANd OrderDate < '1990-01-01';

SELECT COUNT(*)
FROM Stage_Orders_1980;

GO


SELECT TOP (1000) *
FROM dbo.OrdersColumnstorePartitioned
WHERE OrderDate >= '1980-01-01' 
ANd OrderDate < '1990-01-01';

