# Intro to Partitioning in SQL Server

Large tables in SQL Server present challenges on several fronts. For query performance, avoiding accessing data unnecessarily becomes more important. For ETL, loading and modifying data while utilizing compute resource and avoiding lock contention becomes more difficult. For maintenance, making efficient use of resources and time presents challenges. 

In this session, we will introduce the core concepts of partitioning in SQL Server -- partitioning columns, partition schemes, and partition functions. We will configure heaps, rowstores, and columnstores as partitioned tables, and execute queries to compare how they access data. Finally, we will work with the requirements for switching partitions in and out of tables.

After this session, you will be able to configure partitioned tables, explain how partitioning affects query execution, and switch partitions in and out of tables.

Topics covered:
- Partition functions
- Partition schemes
- Partition heaps, rowstores, and columnstores
- Partition elimination
- Partition switching

Topics that would likely need to be part of a follow up session would include:
- Aligned and non-aligned indexes
- Maintenance
- Statistics
- Effect on query plan
- Partitioned views
